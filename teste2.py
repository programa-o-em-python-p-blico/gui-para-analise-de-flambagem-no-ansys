import pyansys

# run ansys with interactive plotting enabled
mapdl = pyansys.launch_mapdl()

# create a square area using keypoints
mapdl.prep7()
mapdl.k(1, 0, 0, 0)
mapdl.k(2, 1, 0, 0)
mapdl.k(3, 1, 1, 0)
mapdl.k(4, 0, 1, 0)
mapdl.l(1, 2)
mapdl.l(2, 3)
mapdl.l(3, 4)
mapdl.l(4, 1)
mapdl.al(1, 2, 3, 4)

# open up the gui
#mapdl.open_gui()

# it resumes where you left off...
mapdl.et(1, 'MESH200', 6)
mapdl.amesh('all')
mapdl.eplot()