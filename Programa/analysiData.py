class analysi():
    def __init__(self):
        self.type = dict()
        self.materialType = dict()
        self.isoMaterialProps = dict()
        self.orthMaterialProps = dict()
        self.anisoMaterialProps = dict()
    
    def setType(self, _type):
        if _type == 'linear':
            self.type['linear'] = True
            self.type['nlinear'] = False
        elif _type == 'nlinear':
            self.type['linear'] = False
            self.type['nlinear'] = True
    
    def setMaterialType(self, _materialType):
        if _materialType == 'iso':
            self.materialType['iso'] = True
            self.materialType['ortho'] = False
            self.materialType['aniso'] = False
        elif _materialType == 'ortho':
            self.materialType['iso'] = False
            self.materialType['ortho'] = True
            self.materialType['aniso'] = False
        elif _materialType == 'aniso':
            self.materialType['iso'] = False
            self.materialType['ortho'] = False
            self.materialType['aniso'] = True
    
    def materialProps(self, _list):
        if self.materialType['iso']:
            self.isoMaterialProps['E'] = _list[0]
            self.isoMaterialProps['PR'] = _list[1]
            self.isoMaterialProps['DENS'] = _list[2]
        elif self.materialType['ortho']:
            