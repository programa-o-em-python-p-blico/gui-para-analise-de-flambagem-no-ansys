import pyansys
import os

ansys = pyansys.launch_mapdl(interactive_plotting=False, run_location = os.getcwd())

# ##############  ANALISE ESTRUTURAL DE VIGA I  ##############

# =======  CONFIGURACOES INICIAIS  =======

ansys.finish()

ansys.run("/CLEAR,START")

ansys.run("/FILNAME,VigaI,0")

# ansys.run("/CWD, 'D:\Documentos\APDL\scripts'")

# =======  DECLARACAO DAS VARIAVEIS  =======

# 1 - Geometria:

t=0.006

bf=0.100

d=0.100

bw=d-t

L=1.2

# 2 - Propriedades do material:

Ex=10000000000

Ey=10000000000

Ez=24000000000

PRxy=0.16

PRyz=0.16

PRxz=0.32

Gxy=3000000000

Gyz=3000000000

Gxz=3000000000

Dens=1800

# =======  PRE-PROCESSAMENTO  =======

# -------  INICIALIZACAO DA FASE DE PRE-PROCESSAMENTO  -------

ansys.run("/PREP7")

# ansys.run("/VIEW,1,1,1,1")

# ansys.run("/TITLE,Viga I - Secao: %d*1000% x %bf*1000% - Comprimento: %L%m")

ansys.run("/REPLOT")

# BCSOPTION,,DEFAULT

# -------  CRIACAO DO ELEMENTO FINITO  -------

ansys.et(1, "SHELL181")

ansys.keyopt(1, 1, 0)  #Element stiffness: 0 - Bending and membrane stiffness (default)

ansys.keyopt(1, 3, 2)  #Integration option: 2 - Full integration with incompatible modes

ansys.keyopt(1, 8, 0)  #Specify layer data storage: 0 - Store data for bottom of bottom layer and top of top layer (multi-layer elements) (default)

ansys.keyopt(1, 9, 0)  #User thickness option: 0 - No user subroutine to provide initial thickness (default)

ansys.keyopt(1, 10, 0)  #Curved shell formulation: 0 - Standard shell formulation (default)

# -------  CRIACAO DO MATERIAL  -------

ansys.mp("EX", 1, Ex)

ansys.mp("EY", 1, Ey)

ansys.mp("EZ", 1, Ez)

ansys.mp("PRXY", 1, PRxy)

ansys.mp("PRXZ", 1, PRxz)

ansys.mp("PRYZ", 1, PRyz)

ansys.mp("GXY", 1, Gxy)

ansys.mp("GXZ", 1, Gxz)

ansys.mp("GYZ", 1, Gyz)

ansys.mp("DENS", 1, Dens)

# -------  CRIACAO DA SECAO  -------

ansys.sectype(1, "SHELL", "", "perfil")

ansys.secoffset("MID")

ansys.secdata(t, 1)

# -------  LOCACAO DOS KEYPOINTS  -------

ansys.k(1, 0, 0, 0)

ansys.k(2, bf/2, 0, 0)

ansys.k(3, bf, 0, 0)

ansys.k(4, 0, bw, 0)

ansys.k(5, bf/2, bw, 0)

ansys.k(6, bf, bw, 0)

ansys.k(101, 0, 0, L/3)

ansys.k(102, bf/2, 0, L/3)

ansys.k(103, bf, 0, L/3)

ansys.k(104, 0, bw, L/3)

ansys.k(105, bf/2, bw, L/3)

ansys.k(106, bf, bw, L/3)

ansys.k(201, 0, 0, 2*L/3)

ansys.k(202, bf/2, 0, 2*L/3)

ansys.k(203, bf, 0, 2*L/3)

ansys.k(204, 0, bw, 2*L/3)

ansys.k(205, bf/2, bw, 2*L/3)

ansys.k(206, bf, bw, 2*L/3)

ansys.k(301, 0, 0, L)

ansys.k(302, bf/2, 0, L)

ansys.k(303, bf, 0, L)

ansys.k(304, 0, bw, L)

ansys.k(305, bf/2, bw, L)

ansys.k(306, bf, bw, L)

# -------  CRIACAO DAS AREAS  -------

ansys.a(1, 2, 102, 101)

ansys.a(2, 3, 103, 102)

ansys.a(4, 5, 105, 104)

ansys.a(5, 6, 106, 105)

ansys.a(2, 5, 105, 102)

ansys.a(101, 102, 202, 201)

ansys.a(102, 103, 203, 202)

ansys.a(104, 105, 205, 204)

ansys.a(105, 106, 206, 205)

ansys.a(102, 105, 205, 202)

ansys.a(201, 202, 302, 301)

ansys.a(202, 203, 303, 302)

ansys.a(204, 205, 305, 304)

ansys.a(205, 206, 306, 305)

ansys.a(202, 205, 305, 302)


# -------  GERACAO DE ELEMENTO E MALHA  -------

ansys.asel("ALL")

ansys.aatt(1, 1, 1, 0, 1)

ansys.mshkey(1)

ansys.aesize("ALL", 0.01)

ansys.amesh("ALL")


# -------  FINALIZACAO DA FASE DE PRE-PROCESSAMENTO -------

ansys.finish()

# =======  PROCESSAMENTO  =======

# -------  INICIALIZACAO DA FASE DE PROCESSAMENTO  -------

ansys.run("/SOLU")

# -------  DEFINICAO DAS RESTRICOES DE GRAU DE LIBERDADE  -------

ansys.nsel("S", "LOC", "Z", 0)

ansys.nsel("R", "LOC", "X", 0, bf)

ansys.nsel("R", "LOC", "Y", 0)

ansys.d("ALL", "UX", 0)

ansys.d("ALL", "UY", 0)

ansys.d("ALL", "UZ", 0)



ansys.nsel("S", "LOC", "Z", L)

ansys.nsel("R", "LOC", "X", 0, bf)

ansys.nsel("R", "LOC", "Y", 0)

ansys.d("ALL", "UX", 0)

ansys.d("ALL", "UY", 0)


ansys.nsel("S", "LOC", "Z", 0)

ansys.nsel("R", "LOC", "X", 0, bf)

ansys.nsel("R", "LOC", "Y", bw)

ansys.d("ALL", "UX", 0)

ansys.d("ALL", "ROTZ", 0)


ansys.nsel("S", "LOC", "Z", L)

ansys.nsel("R", "LOC", "X", 0, bf)

ansys.nsel("R", "LOC", "Y", bw)

ansys.d("ALL", "UX", 0)

ansys.d("ALL", "ROTZ", 0)


# NSEL,NONE

# -------  DEFINICAO DO CARREGAMENTO  -------

ansys.fk(105, "FY", -1)

ansys.fk(205, "FY", -1)

# -------  ANALISE ESTATICA  -------
ansys.finish()
ansys.allsel()
# ansys.open_gui()
ansys.run("/SOLU")

ansys.antype("STATIC")

# ansys.pstres("ON")

# ansys.allsel()
# ansys.open_gui()
ansys.solve()

ansys.finish()

# -------  ANALISE DE ESTABILIDADE ELASTICA (FLAMBAGEM)  -------

ansys.run("/SOLU")

ansys.antype("BUCKLE")

ansys.bucopt("LANB", 10)

ansys.solve()

ansys.finish()

# =======  POS-PROCESSAMENTO  =======

# -------  INICIALIZACAO DA FASE DE POS-PROCESSAMENTO  -------

ansys.exit()


result = pyansys.read_binary('VigaI.rst')

# return a dictionary of solution info for the first result
print()
print('='*50)
print()
for i in range(10):
    info = result.solution_info(i)
    print(f'MODO {i}')
    print(f'   Fact: {info["timfrq"]}')
    print('-'*30)

# for key in info:
#     print(key, info[key])