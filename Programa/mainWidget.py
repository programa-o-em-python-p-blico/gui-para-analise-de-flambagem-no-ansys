from PyQt5.QtWidgets import (QWidget, QHBoxLayout, QRadioButton, QVBoxLayout,
                             QGroupBox, QPushButton, QComboBox)
from PyQt5 import QtCore, QtGui
import inputData
import json


class MainWidget(QWidget):
    def __init__(self):
        super(MainWidget, self).__init__()

        self.analisy = dict()

        self.gui()

    def gui(self):

        self.text = self.getTextLabels()

        # Tipo de análise
        self.analyseType_gBox = QGroupBox(
            self.text["mainWidget"]["gBoxTitle1"])
        self.analyseType_gBox.setFixedSize(180, 110)
        self.analyseType_vbox = QVBoxLayout()
        # self.analyseType_vbox.setAlignment(QtCore.Qt.AlignHCenter)
        self.linear_radioButton = QRadioButton(
            self.text["mainWidget"]["radioOption1"])
        self.nLinear_radioButton = QRadioButton(
            self.text["mainWidget"]["radioOption2"])

        self.analyseType_vbox.addWidget(self.linear_radioButton)
        self.analyseType_vbox.addWidget(self.nLinear_radioButton)

        self.analyseType_gBox.setLayout(self.analyseType_vbox)

        # Propriedades do material
        self.materialProps_gBox = QGroupBox(
            self.text["mainWidget"]["gBoxTitle2"])
        self.materialProps_gBox.setFixedSize(400, 110)
        self.materialProps_vBox = QVBoxLayout()
        self.materialProps_hbox = QHBoxLayout()
        self.isotropic_radioButton = QRadioButton(
            self.text["mainWidget"]["radioOption3"])
        self.orthotropic_radioButton = QRadioButton(
            self.text["mainWidget"]["radioOption4"])
        self.anisotropic_radioButton = QRadioButton(
            self.text["mainWidget"]["radioOption5"])

        self.materialProps_hbox.addWidget(self.isotropic_radioButton)
        self.materialProps_hbox.addWidget(self.orthotropic_radioButton)
        self.materialProps_hbox.addWidget(self.anisotropic_radioButton)

        self.define_button = QPushButton(self.text["mainWidget"]["button1"])
        self.define_button.setFixedSize(120, 25)
        self.define_button.clicked.connect(self.inputMaterialProps)
        self.define_hbox = QHBoxLayout()
        self.define_hbox.addWidget(self.define_button)
        self.define_hbox.setAlignment(QtCore.Qt.AlignCenter)

        self.materialProps_vBox.addItem(self.materialProps_hbox)
        self.materialProps_vBox.addItem(self.define_hbox)

        self.materialProps_gBox.setLayout(self.materialProps_vBox)

        # Geometria
        self.geometry_gBox = QGroupBox(self.text["mainWidget"]["gBoxTitle3"])
        self.geometry_gBox.setFixedSize(595, 160)
        self.geometry_hbox = QHBoxLayout()

        self.Isection_button = QPushButton()
        self.Isection_button.setIcon(QtGui.QIcon('Programa/icons/vigaI.png'))
        self.Isection_button.setIconSize(QtCore.QSize(60, 60))
        self.Isection_button.setFixedSize(80, 80)

        self.tubularSection_button = QPushButton()
        self.tubularSection_button.setIcon(
            QtGui.QIcon('Programa/icons/vigaTubular.png'))
        self.tubularSection_button.setIconSize(QtCore.QSize(60, 60))
        self.tubularSection_button.setFixedSize(80, 80)

        self.cSection_button = QPushButton()
        self.cSection_button.setIcon(
            QtGui.QIcon('Programa/icons/vigaC.png'))
        self.cSection_button.setIconSize(QtCore.QSize(60, 60))
        self.cSection_button.setFixedSize(80, 80)

        self.uSection_button = QPushButton()
        self.uSection_button.setIcon(
            QtGui.QIcon('Programa/icons/vigaU.png'))
        self.uSection_button.setIconSize(QtCore.QSize(60, 60))
        self.uSection_button.setFixedSize(80, 80)

        self.rackSction_button = QPushButton()
        self.rackSction_button.setIcon(
            QtGui.QIcon('Programa/icons/rack.png'))
        self.rackSction_button.setIconSize(QtCore.QSize(60, 60))
        self.rackSction_button.setFixedSize(80, 80)

        self.angleSction_button = QPushButton()
        self.angleSction_button.setIcon(
            QtGui.QIcon('Programa/icons/angle.png'))
        self.angleSction_button.setIconSize(QtCore.QSize(60, 60))
        self.angleSction_button.setFixedSize(80, 80)

        self.geometry_hbox.addWidget(self.Isection_button)
        self.geometry_hbox.addWidget(self.tubularSection_button)
        self.geometry_hbox.addWidget(self.cSection_button)
        self.geometry_hbox.addWidget(self.uSection_button)
        self.geometry_hbox.addWidget(self.rackSction_button)
        self.geometry_hbox.addWidget(self.angleSction_button)

        self.geometry_gBox.setLayout(self.geometry_hbox)

        # Discretização
        self.discretization_gBox = QGroupBox(
            self.text["mainWidget"]["gBoxTitle4"])
        self.discretization_gBox.setFixedSize(200, 150)
        self.discretization_vBox = QVBoxLayout()
        self.discretization_hbox = QHBoxLayout()
        self.discretization_hbox.setAlignment(QtCore.Qt.AlignVCenter)
        self.triangle_radioButton = QRadioButton(
            self.text["mainWidget"]["radioOption6"])
        self.rectangle_radioButton = QRadioButton(
            self.text["mainWidget"]["radioOption7"])

        self.discretization_hbox.addWidget(self.triangle_radioButton)
        self.discretization_hbox.addWidget(self.rectangle_radioButton)

        self.config_button = QPushButton(self.text["mainWidget"]["button3"])
        self.config_button.setFixedSize(120, 25)
        self.config_hbox = QHBoxLayout()
        self.config_hbox.addWidget(self.config_button)
        self.config_hbox.setAlignment(QtCore.Qt.AlignCenter)

        self.discretization_vBox.addItem(self.discretization_hbox)
        self.discretization_vBox.addItem(self.config_hbox)

        self.discretization_gBox.setLayout(self.discretization_vBox)

        # Condições de contorno
        self.boundaryConditions_gBox = QGroupBox(
            self.text["mainWidget"]["gBoxTitle5"])
        self.boundaryConditions_gBox.setFixedSize(160, 150)
        self.boundaryConditions_vbox = QVBoxLayout()
        self.options_comboBox = QComboBox()
        self.personalize_button = QPushButton(
            self.text["mainWidget"]["button2"])
        self.personalize_button.setFixedSize(120, 25)

        self.boundaryConditions_vbox.addWidget(self.options_comboBox)
        self.boundaryConditions_vbox.addWidget(self.personalize_button)

        self.boundaryConditions_gBox.setLayout(self.boundaryConditions_vbox)

        # Carregamento
        self.load_gBox = QGroupBox(self.text["mainWidget"]["gBoxTitle6"])
        self.load_gBox.setFixedSize(200, 150)
        self.load_vBox = QVBoxLayout()
        self.load_hbox = QHBoxLayout()
        self.load_hbox.setAlignment(QtCore.Qt.AlignVCenter)
        self.bending_radioButton = QRadioButton(
            self.text["mainWidget"]["radioOption8"])
        self.axialLoad_radioButton = QRadioButton(
            self.text["mainWidget"]["radioOption9"])

        self.load_hbox.addWidget(self.bending_radioButton)
        self.load_hbox.addWidget(self.axialLoad_radioButton)

        self.config_button = QPushButton(self.text["mainWidget"]["button3"])
        self.config_button.setFixedSize(120, 25)
        self.config_hbox = QHBoxLayout()
        self.config_hbox.addWidget(self.config_button)
        self.config_hbox.setAlignment(QtCore.Qt.AlignCenter)

        self.load_vBox.addItem(self.load_hbox)
        self.load_vBox.addItem(self.config_hbox)

        self.load_gBox.setLayout(self.load_vBox)

        # Etapa final
        self.end_hBox = QHBoxLayout()
        self.end_hBox.setAlignment(QtCore.Qt.AlignRight)

        self.solve_button = QPushButton(self.text["mainWidget"]["button4"])
        self.solve_button.setFixedSize(150, 30)
        self.view_button = QPushButton(self.text["mainWidget"]["button5"])
        self.view_button.setFixedSize(150, 30)

        self.end_hBox.addWidget(self.solve_button)
        self.end_hBox.addWidget(self.view_button)

        # Escopo global
        self.global_vbox = QVBoxLayout()
        self.block1_hbox = QHBoxLayout()
        self.block2_hbox = QHBoxLayout()
        self.block3_hbox = QHBoxLayout()

        self.block1_hbox.addWidget(self.analyseType_gBox)
        self.block1_hbox.addWidget(self.materialProps_gBox)
        self.block2_hbox.addWidget(self.geometry_gBox)
        self.block3_hbox.addWidget(self.discretization_gBox)
        self.block3_hbox.addWidget(self.boundaryConditions_gBox)
        self.block3_hbox.addWidget(self.load_gBox)

        self.global_vbox.addItem(self.block1_hbox)
        self.global_vbox.addItem(self.block2_hbox)
        self.global_vbox.addItem(self.block3_hbox)
        self.global_vbox.addItem(self.end_hBox)

        self.setLayout(self.global_vbox)

    def inputMaterialProps(self):
        if self.isotropic_radioButton.isChecked():
            self.inputIsotropicMaterialProps()
        elif self.orthotropic_radioButton.isChecked():
            self.inputOrthotropicMaterialProps()
        elif self.anisotropic_radioButton.isChecked():
            self.inputAnisotropicMaterialProps()

    def inputIsotropicMaterialProps(self):
        inputWindow = inputData.InputIsotropicMaterialProps(self)
        inputWindow.show()

    def inputOrthotropicMaterialProps(self):
        inputWindow = inputData.InputOrthotropicMaterialProps(self)
        inputWindow.show()

    def inputAnisotropicMaterialProps(self):
        inputWindow = inputData.InputAnisotropicMaterialProps(self)
        inputWindow.show()

    def getTextLabels(self):

        with open('Programa/portuguesText.json',
             'r', encoding='utf8') as jfile:
            text = json.load(jfile)

        return text
