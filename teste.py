import pyansys
import os

ansys = pyansys.launch_mapdl(interactive_plotting=True, run_location= os.getcwd())
# ansys.gfile(1200)
ansys.prep7()
ansys.k(1, 0, 0, 0)
ansys.k(2, 10, 0, 0)
ansys.k(3, 20, 0, 0)
ansys.k(4, 30, 0, 0)
ansys.view(1, 1, 1, 1)

ansys.kplot()