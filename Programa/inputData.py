import json
from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import (QDialog, QVBoxLayout, QGridLayout, QLineEdit,
                             QLabel, QPushButton, QHBoxLayout)


class InputLinearAnalisyProps(QDialog):
    def __init__(self):
        super().__init__(QtCore.Qt.WindowSystemMenuHint |
                         QtCore.Qt.WindowTitleHint |
                         QtCore.Qt.WindowCloseButtonHint)

        self.text = getTextLabels()

        self.title = self.text['InputLinearAnalisyProps']['windowTitle']
        self.left = 650
        self.top = 250
        self.width = 200
        self.height = 260
        self.icone = ''

        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon(self.icone))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.vbox = QVBoxLayout()

        self.setWindowModality(QtCore.Qt.ApplicationModal)

        self.setLayout(self.vbox)

    def gui(self):

        self.grid = QGridLayout()


class InputNonLinearAnalisyProps(QDialog):
    def __init__(self):
        super().__init__(QtCore.Qt.WindowSystemMenuHint |
                         QtCore.Qt.WindowTitleHint |
                         QtCore.Qt.WindowCloseButtonHint)

        self.text = getTextLabels()

        self.title = self.text['InputNonLinearAnalisyProps']['windowTitle']
        self.left = 650
        self.top = 250
        self.width = 200
        self.height = 260
        self.icone = ''

        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon(self.icone))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.vbox = QVBoxLayout()

        self.setWindowModality(QtCore.Qt.ApplicationModal)

        self.setLayout(self.vbox)

    def gui(self):

        self.grid = QGridLayout()


class InputIsotropicMaterialProps(QDialog):
    def __init__(self, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint |
                         QtCore.Qt.WindowTitleHint |
                         QtCore.Qt.WindowCloseButtonHint)

        self.text = getTextLabels(self)

        self.title = self.text['InputIsotropicMaterialProps']['windowTitle']
        self.left = 550
        self.top = 200
        self.width = 170
        self.height = 130
        self.icone = ''

        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon(self.icone))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.vbox = QVBoxLayout()

        self.setWindowModality(QtCore.Qt.ApplicationModal)

        self.gui()

        self.setLayout(self.vbox)

    def gui(self):

        self.grid = QGridLayout()

        self.elasticModulus_lineEdit = QLineEdit()
        self.elasticModulus_lineEdit.setFixedSize(100, 25)
        self.elasticModulus_label = QLabel('E')
        self.elasticModulus_label.setFixedSize(20, 40)
        self.poison_lineEdit = QLineEdit()
        self.poison_lineEdit.setFixedSize(100, 25)
        self.poison_label = QLabel('v')
        self.poison_label.setFixedSize(20, 40)

        self.grid.addWidget(
            self.elasticModulus_label, 0, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.elasticModulus_lineEdit, 0, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(
            self.poison_label, 1, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.poison_lineEdit, 1, 1, QtCore.Qt.AlignLeft)

        self.ok_button = QPushButton(
            self.text['InputIsotropicMaterialProps']['button1'])
        self.cancel_button = QPushButton(
            self.text['InputIsotropicMaterialProps']['button2'])
        self.verticalSpace = QLabel()
        self.verticalSpace.setFixedHeight(20)

        self.hbox = QHBoxLayout()
        self.hbox.setAlignment(QtCore.Qt.AlignRight)

        self.hbox.addWidget(self.ok_button)
        self.hbox.addWidget(self.cancel_button)

        self.vbox.addItem(self.grid)
        self.vbox.addWidget(self.verticalSpace)
        self.vbox.addItem(self.hbox)

        self.cancel_button.clicked.connect(self.cancel)

    def cancel(self):
        self.close()


class InputOrthotropicMaterialProps(QDialog):
    def __init__(self, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint |
                         QtCore.Qt.WindowTitleHint |
                         QtCore.Qt.WindowCloseButtonHint)

        self.text = getTextLabels(self)

        self.title = self.text['InputOrthotropicMaterialProps']['windowTitle']
        self.left = 550
        self.top = 200
        self.width = 170
        self.height = 300
        self.icone = ''

        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon(self.icone))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.vbox = QVBoxLayout()

        self.setWindowModality(QtCore.Qt.ApplicationModal)

        self.gui()

        self.setLayout(self.vbox)

    def gui(self):

        self.grid = QGridLayout()

        self.elasticModulus_lineEdit1 = QLineEdit()
        self.elasticModulus_lineEdit1.setFixedSize(100, 25)
        self.elasticModulus_label1 = QLabel('Ex')
        self.elasticModulus_label1.setFixedSize(20, 30)

        self.elasticModulus_lineEdit2 = QLineEdit()
        self.elasticModulus_lineEdit2.setFixedSize(100, 25)
        self.elasticModulus_label2 = QLabel('Ey')
        self.elasticModulus_label2.setFixedSize(20, 30)

        self.elasticModulus_lineEdit3 = QLineEdit()
        self.elasticModulus_lineEdit3.setFixedSize(100, 25)
        self.elasticModulus_label3 = QLabel('Ez')
        self.elasticModulus_label3.setFixedSize(20, 30)

        self.poison_lineEdit1 = QLineEdit()
        self.poison_lineEdit1.setFixedSize(100, 25)
        self.poison_label1 = QLabel('vxy')
        self.poison_label1.setFixedSize(20, 30)

        self.poison_lineEdit2 = QLineEdit()
        self.poison_lineEdit2.setFixedSize(100, 25)
        self.poison_label2 = QLabel('vyz')
        self.poison_label2.setFixedSize(20, 30)

        self.poison_lineEdit3 = QLineEdit()
        self.poison_lineEdit3.setFixedSize(100, 25)
        self.poison_label3 = QLabel('vxz')
        self.poison_label3.setFixedSize(20, 30)

        self.shearModulus_lineEdit1 = QLineEdit()
        self.shearModulus_lineEdit1.setFixedSize(100, 25)
        self.shearModulus_label1 = QLabel('Gxy')
        self.shearModulus_label1.setFixedSize(20, 30)

        self.shearModulus_lineEdit2 = QLineEdit()
        self.shearModulus_lineEdit2.setFixedSize(100, 25)
        self.shearModulus_label2 = QLabel('Gyz')
        self.shearModulus_label2.setFixedSize(20, 30)

        self.shearModulus_lineEdit3 = QLineEdit()
        self.shearModulus_lineEdit3.setFixedSize(100, 25)
        self.shearModulus_label3 = QLabel('Gxz')
        self.shearModulus_label3.setFixedSize(20, 30)

        self.grid.addWidget(
            self.elasticModulus_label1, 0, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.elasticModulus_lineEdit1, 0, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(
            self.elasticModulus_label2, 1, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.elasticModulus_lineEdit2, 1, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(
            self.elasticModulus_label3, 2, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.elasticModulus_lineEdit3, 2, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(
            self.poison_label1, 3, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.poison_lineEdit1, 3, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(
            self.poison_label2, 4, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.poison_lineEdit2, 4, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(
            self.poison_label3, 5, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.poison_lineEdit3, 5, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(
            self.shearModulus_label1, 6, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.shearModulus_lineEdit1, 6, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(
            self.shearModulus_label2, 7, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.shearModulus_lineEdit2, 7, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(
            self.shearModulus_label3, 8, 0, QtCore.Qt.AlignCenter)
        self.grid.addWidget(
            self.shearModulus_lineEdit3, 8, 1, QtCore.Qt.AlignLeft)

        self.ok_button = QPushButton(
            self.text['InputOrthotropicMaterialProps']['button1'])
        self.cancel_button = QPushButton(
            self.text['InputOrthotropicMaterialProps']['button2'])
        self.verticalSpace = QLabel()
        self.verticalSpace.setFixedHeight(20)

        self.hbox = QHBoxLayout()
        self.hbox.setAlignment(QtCore.Qt.AlignRight)

        self.hbox.addWidget(self.ok_button)
        self.hbox.addWidget(self.cancel_button)

        self.vbox.addItem(self.grid)
        self.vbox.addWidget(self.verticalSpace)
        self.vbox.addItem(self.hbox)

        self.cancel_button.clicked.connect(self.cancel)

    def cancel(self):
        self.close()


class InputAnisotropicMaterialProps(QDialog):
    def __init__(self, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint |
                         QtCore.Qt.WindowTitleHint |
                         QtCore.Qt.WindowCloseButtonHint)

        self.text = getTextLabels(self)

        self.title = self.text['InputAnisotropicMaterialProps']['windowTitle']
        self.left = 450
        self.top = 200
        self.width = 800
        self.height = 250
        self.icone = ''

        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon(self.icone))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.vbox = QVBoxLayout()

        self.setWindowModality(QtCore.Qt.ApplicationModal)

        self.gui()

        self.setLayout(self.vbox)

    def gui(self):

        self.grid = QGridLayout()

        self.componentLineEdit_list = list()
        self.componentLabel_list = list()
        cont = 0
        for i in range(6):
            for j in range(6):
                self.elasticComponent_lineEdit = QLineEdit()
                self.elasticComponent_lineEdit.setFixedSize(80, 25)
                self.elasticComponent_label = QLabel(f'D{i+1}{j+1}')
                self.elasticComponent_label.setFixedSize(20, 30)

                self.componentLineEdit_list.append(
                    self.elasticComponent_lineEdit)
                self.componentLabel_list.append(self.elasticComponent_label)

                self.grid.addWidget(
                    self.componentLabel_list[cont], i, 2*j,
                    QtCore.Qt.AlignCenter)
                self.grid.addWidget(
                    self.componentLineEdit_list[cont], i, 2*j+1,
                    QtCore.Qt.AlignLeft)

                cont += 1

        self.ok_button = QPushButton(
            self.text['InputAnisotropicMaterialProps']['button1'])
        self.cancel_button = QPushButton(
            self.text['InputAnisotropicMaterialProps']['button2'])
        self.verticalSpace = QLabel()
        self.verticalSpace.setFixedHeight(20)

        self.hbox = QHBoxLayout()
        self.hbox.setAlignment(QtCore.Qt.AlignRight)

        self.hbox.addWidget(self.ok_button)
        self.hbox.addWidget(self.cancel_button)

        self.vbox.addItem(self.grid)
        self.vbox.addWidget(self.verticalSpace)
        self.vbox.addItem(self.hbox)

        self.cancel_button.clicked.connect(self.cancel)

    def cancel(self):
        self.close()


def getTextLabels(self):

    with open('Programa/portuguesText.json',
              'r', encoding='utf8') as jfile:
        text = json.load(jfile)

    return text
