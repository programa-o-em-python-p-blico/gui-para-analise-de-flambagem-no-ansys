import json
from PyQt5.QtWidgets import QMainWindow
from PyQt5 import QtGui
from mainWidget import MainWidget


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.text = self.getTextLabels()

        self.windowTitle = self.text['mainWindow']['windowTitle']
        self.left = 300
        self.top = 50
        self.width = 650
        self.height = 600
        self.icon = ''

        self.setWindowTitle(self.windowTitle)
        self.setWindowIcon(QtGui.QIcon(self.icon))
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.mainWidget = MainWidget()
        self.setCentralWidget(self.mainWidget)

    def getTextLabels(self):

        with open('Programa/portuguesText.json',
             'r', encoding='utf8') as jfile:
            text = json.load(jfile)

        return text
