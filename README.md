# GUI para análise de flambagem no ANSYS

Interface gráfica amigável para realização de análises de flambagem linear e não-linear em vigas e colunas utilizando o software de análise numérica ANSYS.