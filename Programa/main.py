from PyQt5.QtWidgets import QApplication
from mainWindow import MainWindow
import sys

if __name__ == '__main__':
    App = QApplication([])
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(App.exec_())
